DIR = ./
TARGET = simple_shell
simple_shell: simple_shell.o
	gcc -o  simple_shell simple_shell.o
simple_shell.o: simple_shell.c
	gcc -c simple_shell.c
clean:
	rm -rf $(DIR)*.o
	rm -rf $(TARGET)

