/**********************************************************************************************************************************************************
 * Simple UNIX Shell
 * @author: 
 * 
 **/


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/wait.h> 
#include <sys/stat.h> 
#include <fcntl.h> 

#define MAX_LENGTH 80 // The maximum length of the commands
void parseCommand(char* command, char** args,int* special_operation){
	char *temp=command;
	int i = 0;
	//tokenize command into args
	do{
		args[i] = strtok_r(temp, " \n", &temp);
		if (args[i]==NULL)
			break;
		if (strcmp(args[i],">")==0)
			special_operation[1]=i;
		else if(strcmp(args[i],"<")==0)
			special_operation[2]=i;
		else if (strcmp(args[i],"|")==0)
			special_operation[3]=i; 
		
		++i;
	}while (1);
	special_operation[0]=i;
}

void execute(char** parsed, int isBackground) 
{ 
    // Forking a child 
    pid_t pid = fork();  
  
    if (pid == -1) { 
        printf("Failed forking child..\n"); 
        return; 
    } else if (pid == 0) { 
		// In child process
		
        if (execvp(parsed[0], parsed) < 0) { 
            printf("Could not execute command..\n"); 
        } 
		exit(0);
    } 
	else
	//In parent process
	{
		if (isBackground==0)
		{
			wait(NULL);
		}
		return;  
	}
}
//If command contains pipe argument
		//	fork a child process invoking fork() system call and perform the followings in the child process:
		//		create a pipe invoking pipe() system call
		//		fork another child process invoking fork() system call and perform the followings in this child process:
		//			close the write end descriptor of the pipe invoking close() system call
		//			copy the read end  descriptor of the pipe to standard input file descriptor (STDIN_FILENO) invoking dup2() system call
		//			change the process image of the this child with the new image according to the second UNIX command after the pipe symbol (|) using execvp() system call
		//		close the read end descriptor of the pipe invoking close() system call
		//		copy the write end descriptor of the pipe to standard output file descriptor (STDOUT_FILENO) invoking dup2() system call
		//		change the process image with the new process image according to the first UNIX command before the pipe symbol (|) using execvp() system call
		//	If command does not conatain & (ampersand) at the end
		//		invoke wait() system call in parent process.
void executePipedCmd(char** args,char** pipedArgs,int isBackground){
	int fds[2];
	if(pipe(fds)<0){
		printf("Pipe could not be initialized\n"); 
        return; 
	}
	pid_t pid1,pid2;
	pid1=fork();
	if (pid1 == -1) { 
        printf("Failed forking child..\n"); 
		return;
    } else if (pid1 == 0) { 
		close(fds[0]);
		dup2(fds[1],STDOUT_FILENO);
	
		if (execvp(args[0], args) < 0) { 
        	printf("Could not execute comman.d.\n"); 
        }
		close(fds[1]);
	}else{
		close(fds[1]);
		dup2(fds[0],STDIN_FILENO);
		
		if (execvp(pipedArgs[0], pipedArgs) < 0) { 
        	printf("Could not execute command..\n"); 
        }
		close(fds[0]);
	}
}
void executeRedir(char** parsed, int isBackground,char* filedir,int type){
	pid_t pid = fork();  
    if (pid == -1) { 
        printf("Failed forking child..\n"); 
		return;
    } else if (pid == 0) { 
		// In child process
		int fd;
		if (type ==0) //output redirection
			fd = open(filedir,O_CREAT|O_WRONLY,S_IRUSR|S_IWUSR);
		else// type = 1 will be input redirection
			fd = open(filedir,O_RDONLY);
		if (fd < 0)
		{
			printf("Could not create file: %s\n",filedir);
			return;
		}
		if (type == 0) //output redirection
		{
			dup2(fd,STDOUT_FILENO);
		}else{ // type = 1 will be input redirection
			dup2(fd,STDIN_FILENO);
		}
		
		
        if (execvp(parsed[0], parsed) < 0) { 
            printf("Could not execute command..\n"); 
		}
		close(fd);
		exit(0);
    } 
	else
	//In parent process
	{
		if (isBackground==0)
		{
			int status;
			while (wait(&status) != pid)
				;
		}
		return;  
	}
}

void processPipedArgs(char** args,char **pipedArgs,int pipeIndex)
{
	args[pipeIndex]=NULL;
	int argsIndex = pipeIndex+1;
	int pipedArgsIndex = 0;
	//clone cmd after pipe operation into piped arguments
	while (args[argsIndex]!=NULL)
	{
		pipedArgs[pipedArgsIndex]=args[argsIndex];
		args[argsIndex]=NULL;
		++argsIndex;
		++pipedArgsIndex;
	}
	pipedArgs[pipedArgsIndex]=NULL;
}
int main(void) {
	
	char command[MAX_LENGTH];
	char history[MAX_LENGTH]="";
	char *args[MAX_LENGTH/2 + 1]; // MAximum 40 argments
	char *pipedArgs[MAX_LENGTH/2 +1];
	int should_run = 1;
	int isBackground;
	while (should_run) {
		printf("ssh>>");
		fflush(stdout);
		fgets(command, MAX_LENGTH, stdin);
		//special_operator[0] : argument count
		//special_operator[1] : index of output redirection >
		//special_operator[2] : index of input redirection <
		//special_operator[3] : index of pipe operation |
		int special_operator[4] = {-1,-1,-1,-1};
		//Parse command and arguments.
		parseCommand(command ,args,special_operator);
		if(special_operator[0] == 0) continue;
		if (strcmp(args[0],"!!")==0)
		{
			if(strcmp(history,"")==0)
			{
				printf("There is not any recent command\n");
				continue;
			}
			else
			{
				strcpy(command,history);
			}
		}
		else
			strcpy(history,command);
		if(strcmp(args[0],"exit")==0){
			printf("Goodbye\n");
			should_run=0;
			continue;
		}
		//If command contains output redirection argument
		//	fork a child process invoking fork() system call and perform the followings in the child process:
		//		open the redirected file in write only mode invoking open() system call
		//		copy the opened file descriptor to standard output file descriptor (STDOUT_FILENO) invoking dup2() system call
		//		close the opened file descriptor invoking close() system call
		//		change the process image with the new process image according to the UNIX command using execvp() system call
		//	If command does not conatain & (ampersand) at the end
		//		invoke wait() system call in parent process.
		if (special_operator[1]>=0)
		{
			args[special_operator[1]]=NULL;
			if (strcmp(args[special_operator[0]-1],"&")==0)
			{
			
				args[special_operator[0]-1] = NULL;
				isBackground=1;
			}
			else 
			{
				isBackground = 0;
			}
			executeRedir(args,isBackground,args[special_operator[1]+1],0);
			
		}
		//If command contains input redirection argument
		//	fork a child process invoking fork() system call and perform the followings in the child process:
		//		open the redirected file in read  only mode invoking open() system call
		//		copy the opened file descriptor to standard input file descriptor (STDIN_FILENO) invoking dup2() system call
		//		close the opened file descriptor invoking close() system call
		//		change the process image with the new process image according to the UNIX command using execvp() system call
		//	If command does not conatain & (ampersand) at the end
		//		invoke wait() system call in parent process.
		//
		//	
		else if (special_operator[2]>=0)
		{
			args[special_operator[2]]=NULL;
			if (strcmp(args[special_operator[0]-1],"&")==0)
			{
				
				args[special_operator[0]-1] = NULL;
				isBackground=1;
			}
			else 
			{
				isBackground = 0;
			}
			executeRedir(args,isBackground,args[special_operator[2]+1],1);
		}
		//If command contains pipe argument
		//	fork a child process invoking fork() system call and perform the followings in the child process:
		//		create a pipe invoking pipe() system call
		//		fork another child process invoking fork() system call and perform the followings in this child process:
		//			close the write end descriptor of the pipe invoking close() system call
		//			copy the read end  descriptor of the pipe to standard input file descriptor (STDIN_FILENO) invoking dup2() system call
		//			change the process image of the this child with the new image according to the second UNIX command after the pipe symbol (|) using execvp() system call
		//		close the read end descriptor of the pipe invoking close() system call
		//		copy the write end descriptor of the pipe to standard output file descriptor (STDOUT_FILENO) invoking dup2() system call
		//		change the process image with the new process image according to the first UNIX command before the pipe symbol (|) using execvp() system call
		//	If command does not conatain & (ampersand) at the end
		//		invoke wait() system call in parent process.
		else if (special_operator[3]>0)
		{
			printf("Go into do piped\n");
			printf("Argc: %d\n",special_operator[0]);
			if (strcmp(args[special_operator[0]-1],"&")==0)
			{
				special_operator[0]-=1;
				args[special_operator[0]-1] = NULL;
				isBackground=1;
			}
			else 
			{
				isBackground = 0;
			}
			processPipedArgs(args,pipedArgs,special_operator[3]);
			pid_t pid = fork();
			if(pid ==0)
			{
				executePipedCmd(args,pipedArgs,isBackground);
				exit(0);
			}
			else {
				if(isBackground ==0)
					wait(NULL);
			}
		}
		//If command does not contain any of the above
		//	fork a child process using fork() system call and perform the followings in the child process.
		//		change the process image with the new process image according to the UNIX command using execvp() system call
		//	If command does not conatain & (ampersand) at the end
		//		invoke wait() system call in parent process.
		else{
			if (strcmp(args[special_operator[0]-1],"&")==0)
			{
			
				args[special_operator[0]-1] = NULL;
				isBackground = 1;
			}
			else 
			{
				isBackground=0;
			}
			execute(args,isBackground);
			

		}
	
	}

	return 0;
}
